_**Ma solution technique d'accès ||Mael Dewulf du 27/10/2021**_
________________________________
**Marque :** `UNOWHY`

**Modèle :** `Y13`

________________________               
**Composants essentiels :** 
> Processeur Intel® Celeron N4120 – 4 cœurs 1,1 GHz (jusqu’à 2,6 Ghz) 

> Mémoire vive de 4 Go

>Capacité de stockage de 64 Go SSD

>Carte graphic  Intel® HD Graphics

>Carte réseau Intel® Dual Band Wireless-AC3165
______________________________________________
**Les connecteurs :**            
- WebCam                       
- Micro                          
- TouchPad                       
- Clavier                        
- Bouton Marche/Arret            
- Lecteur d'empreinte            
- Voyant d'alimentation
- USB-Type C
- USB 3.0
- Port mini HDMI
- Prise Jack
- Emplacement Micro-SD
_________

**Système d'exploittion :**  -->  _Windows™ 10 Pro Education_
______________________
**Logiciels installés :**
 
 <details><summary>Suite office</summary>

-->Licence FreeWare :
-Word (version: 2112)
-Excelle (version:2112)
-PowerPoint (version:2112)
</details>
<details><summary>Moteur de recherche</summary>
-Google Chrome (version: 97.0)
-FireFox (version: 78.14)
-Microdoft Edge (version: 97.0)
</details>
 <details><summary>Autres Applications</summary>
- Mes Granules.fr 
- Pense-betes
- Visual Studio Code
- Notion
- GitHub Dekstop
- GitBash
- Discord
</details>

____________________________

**Possibilités d'évolution matériel ou logiciel**

 _Aucune possibilité a ce jour de changer quoi que ce soit dans le pc niveau hardware mais possibilité de faire les  
 mises à jours niveau software _ 

_________________

**Information logicielle ou matérielle importante**
 
_Ce pc est un pc sous droit administrateur dont le mot de passe est incconu a ce jour et qui me bloque certaines applications, je ne peux accéder a tout le système du pc et aimerai trouver un moyen d'y remédier._

____________________________

**Images :**

![Image](img/PC_TTT.png)
![Image](img/PC_TT.png)
![Image](img/PC_T.png)











