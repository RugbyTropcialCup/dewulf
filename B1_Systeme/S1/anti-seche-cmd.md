## Anti-sèche des commandes Windows / Linux / 22/01/2022
### Mael Dewulf

| Antisèche | Windows | Linux |
| --- | --- | --- |
| Afficher ou on est | pwd | pwd |
| Faire l'alias | alias ..=.. |alias ..=.. |
| Créer un nouveau répertoir | md | / |
| Supprimer un répertoir | rd | rm -r nomdurépertoir |
| Exporter | ... | export |
| Afficher tous les dossiers d'ou on se trouve | Dir | ls |
| Afficher le chemin actuel et se déplacer | cd et [/...] | cd |
| déplacer fichier | move | mv |
| effacer tout | cls | clear / Clear control-L |
| créer un nouveau dossier | md nomdossier | mkdir nomdossier |
| Afficher le contenu d'un fichier texte | type | ... |
| copier | copy | cp anciendoc nouveaudoc |
| supprimer | del | rm |
| rename | rename | rename |
| suppression forcée | rmdir | ... |
| executer en boucle | -t après une commande | -t | 
| créer nouveux fichier | copy con nom_du_fichier.extension | touch fic1.txt |
| ou-suis-je | echo %cd% | echo |
| suppression d'application (fihier.exe ou désinstaleur ?) | sc delete | ... |
| Arrêt de l'ordinateur, redémarrage, deco | Shutdown /s ou /r ou /l | Shutdown + (possible argument)|
| Transformer un fichier avec Pandoc | pandoc anti-seche-cmd.md -f markdown -t latex -s -o anti-seche-cmd.pdf | ... |
