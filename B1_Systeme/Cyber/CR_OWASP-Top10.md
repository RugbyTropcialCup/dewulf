# Compte-Rendu : OWASP (Open Web Application Security Project)

**MarkDown Dewulf Mael 12/05/2022**
---------------------------------------------------------------------------------------------

### | Controle d'accès défaillant |

**Le controle d'acces** permet de vérifier qu'un utilisateur n'accède uniquement qu'a certaines ressources cependant lorsque il est **défaillant** l'utilisateur a des acces a des informations d'autres qu'il ne devrais pas avoir.

**Exemple** : Lors d'une recherche web l'utilisateur va modifier l'url pour avoir accès a d'autres profils utilisateurs 

**Recommandation de l'OWASP** :
- Réfléchir sur le fonctionement du controle d'accès au début de la conception de l'application.
- Toutes demande doit passer obligatoirement par un controle d'accès.
- Appliquer le principe du moindre privilège, qui veux dire avoir un compte différent pour avoir des accès administrateur /ou /basique.

--------------------------------------------------------------------------------------------
### | Défaillance cryptographiques et expositions de données sensibles |

**La défaillance CryptoGrapgique** c'est lorque des applications web ou des API's ne protèges pas asser les **donnés sensibles** et donc des pirates peuvent voler ou modifier ces données mal protégées pour pouvoir ensuite procédé a une usurpation d'identité, utilisé les cartes de crédits ou d'autre crimes a défun mal veillant.

**Exemple**: Un site Web sur lequelle la base de données contient des données sensibles tel que des données bancaires, des données relatives aux soins de santé, les données personelles d'authentification.

**Recommandation de l'OWASP** :
- Classifier les données afin de s'avoir quelles sont les données les plus sensibles à chiffrer.
- Chiffrer les données transmises lors du transport.
- Chiffrer/Crypter les données sensibles lors du stockage.

--------------------------------------------------------------------------------------------

### | Injection |

**Une injection** est une faille se produisant quand une donnée non fiable est envoyée à un interpréteur en tant qu'élément d'une commande ou d'une requete. Les données comprométentes du pirate peuvent duper l'interpréteur pour l'amener à des données qu'il n'aurai pas accès.

**Exemple**: Le pirate peux injecter sur l'url du site du code qui va forcer a afficher toutes les entrées de la base de données qui est reliée a l'application et va pouvoir récupérer des mots de passses.

**Recommandation de l'OWASP** :
- Vérifier les données entrées par un utilisateur et protéger les caractères spéciaux type : (&@#)

--------------------------------------------------------------------------------------------

### | Conception non sécurisée |

**Une conception non sécurisée** a pour faille d'éxposer le code d'une application à certaine violations par un pirate.

**Exemple**: Demander en cas de perte de mot de passe les questions type (Votre première voiture,la couleur de votre chien) qui sont simples a répondre mais qui ne prouve pas que je suis bien le propriétaire du compte.

**Recommandation de l'OWASP** :
- Intégrer la sécurité dans le cycle de vie du développement d'une application avec 2FA ou envoi d'sms.

--------------------------------------------------------------------------------------------

### | Mauvaise configuration de sécurité |

**La mauvaise configuration de sécurité** est le résultat de configuration par défaut non sécurisée de configuration incompplète, d'un stockage dans un cloud ouvert, d'en-tetes HTTP ùal configurés et de message d'erreur verbeux contenant des **informations sensibles**.

**Exemple**: Laisser les port 80 ou 21 ouvert et activer ou encore laisser le traitement des fichiers XML sans vérifier leurs contenus.

**Recommandation de l'OWASP** :
- Avoir une procédure de durcissement des systèmes d'exploitation.
- Avoir une procédure de vérification des permissions.
- Désactiver les services qui ne sont pas utilisés.

--------------------------------------------------------------------------------------------

### | Composants vulnérables et obselètes |

Lors de la conception d'une application, un développeur peut utiliser des **composants obselètes** ou vulnérables ce qui peux constituer une porte d'entrée pour un pirate.

**Exemple**: Un OS ou une Base de Données qui n'est plus a jour.

**Recommandation de l'OWASP** :
- Télécharger les composants/drivers depuis une source fiable.
- Mettre a jour régulièrement les composants.
- Désactiver les composants que l'on utlise pas.
- Suivre régulièrement les publications sur les vulnérabilités.

--------------------------------------------------------------------------------------------

### | Identification et authentification de mauvaise qualité |

C'est application qui ne gère pas bien **les authentifications** et les sessions des utilisateurs qui donc constituent un danger pour le Système d'informations.

**Exemple**: Un utlisateur qui possède des droits qu'il ne devrais pas avoir comme des droits administrateur ou alors, une application qui ne bloque pas l'accès à l'authentification après plusieurs tentatives échouées ce qui donne un nombre illimité de tentatives.

**Recommandation de l'OWASP** :
- Changer le mot de passe par défaut.
- Activer l'authentification a multifacteurs (MFA).
- Journaliser les tentatives d'authentification.
- Mise en place d'une politique de mot de passe.
- Mise en place d'un temporiseur après une succéssion d'authentification échouée.

--------------------------------------------------------------------------------------------

### | Manque d'intégrité des données et du logiciel |

C'est des lors qu'une application s'appuyant sur une bibliothèque d'un module venant d'une **source non fiable** ou alors le téléchargement d'une mise a jour **sans vérifier** au préalable l'intégrité des fichiers.

**Exemple**: Lors d'une mise a jour Ubuntu une commande permet de lire la mise a jour est répondre ok si tout les fichiers sont correcte est non corrompu.

**Recommandation de l'OWASP** :
- Envoyer ses fichiers vers un serveur de log.
- Utiliser un outil de supervision.
- Procédure de réponse d'incidents.

--------------------------------------------------------------------------------------------


### | Supervision et journalisation insuffisantes |

C'est la détéction difficile d'incidents, d'anomalie. En d'autre therme le manque de régularité niveau **supervision d'incidents**, ce qui peux laisser à des intrusions.

**Exemple**: Ne pas mettre en place sur un serveur de log un outil de supervision permettant de remonter les informations en cas de cyber attaques.

**Recommandation de l'OWASP** :
- Envoyer ses fichier vers un serveur de log.
- Utiliser un outil de supervision.
- Procédure de réponse d'incidents.

--------------------------------------------------------------------------------------------

### | Falsification de requetes coté serveur (SSRF=Serveur-Side Request Forgery) |

C'est lorsque l'on force une application a envoyer des requetes vers un domaine spécifique ou un ordinateur spécifique.

**Exemple**: Utiliser un serveur vulnérable pour pouvoir scanner les machines qui sont sur le meme résaux que lui.

**Recommandation de l'OWASP** :
- Journaliser le trafic.
- Vérifier les informations envoyées par un utilisateur.
- Utiliser une liste blanche contenant les protocoles et les adresses IP autorisées.

--------------------------------------------------------------------------------------------

**Sources:** TOP 10 (2021) : https://youtu.be/xe9LN2w7hfE 
            TOP 10 : https://owasp.org/www-project-top-ten/
