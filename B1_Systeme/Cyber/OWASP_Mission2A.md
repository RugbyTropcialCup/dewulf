# Mission : Mutillidae 2

**MarkDown Dewulf Mael 03/07/2022**
---------------------------------------------------
### 🛑 Remarque : Travail effectuer avec le Mutillidae de Madame Abdelmoula  🛑
---------------------------------------------------

### | Travail à faire 1 - Énumération des logins en mode non sécurisé |

- [x] **Q1**. Commencer par installer l’extension Wsdler en réalisant les manipulations décrites dans
**l’étape n°1**. Puis, positionner le **niveau de sécurité à 0**.
--------------------------------------------------------

![Q1](B1_Systeme/img/Owasp2Q1.png)
--------------
![Q1](B1_Systeme/img/T1Q3.png)
----------------
![Q1](B1_Systeme/img/Owasp2Q11.png)
---------------
![Q1](B1_Systeme/img/OwasppQ1.png)

-----------------------------------------------------------

- [x] **Q2**. Tester un exemple de requête et de réponse à l’aide d’un login non valide en réalisant les
manipulations décrites dans **l’étape n°2** (parse de la page wsdl, envoi au répéteur,
génération de la réponse et envoi au comparateur).

-----------------------------------------------------------

![Q2](B1_Systeme/img/Owasp2Q2.png)
------------------------
![Q2](B1_Systeme/img/Owasp2Q22.png)

------------------------------------------------------------

 - [x] **Q3**. Tester un exemple de requête et de réponse à l’aide d’un login valide en réalisant les
manipulations décrites dans **l’étape n°3** (parse de la page wsdl, envoi au répéteur,
modification avec un login valide, génération de la réponse et envoi au comparateur).

-------------------------------------------------------------

![Q2](B1_Systeme/img/Owasp2Q3png.png)
------------------
![Q2](B1_Systeme/img/Owasp2Q33.png)

------------------------------------------------------------

- [x] **Q4**. Créer un dictionnaire de login sur votre machine cliente. Pour cela, ouvrir un éditeur de texte
et saisir des logins les uns en dessous des autres et enregistrer votre fichier.

------------------------------------------------------------

![Q2](B1_Systeme/img/Owasp2Q333.png)

-----------------------------------------------------------

 - [x] **Q5**. Lancer l’énumération et relever les logins valides en réalisant les manipulations décrites
dans **l’étape n°4**.

---------------------------------------------------------

![Q2](B1_Systeme/img/Owasp2Q3333png.png)
------------------
![Q2](B1_Systeme/img/Owasp2Q33333.png)

-------------------------------------------------------

 - [x] **Q6**. A l’aide du comparateur, expliquer quelles sont les lignes de la réponse sur lesquelles
l’attaquant a pu s’appuyer pour lancer l’attaque ?

- Les lignes sur lesquelles l'attaquant a pu s'appuyer pour lancer l'attaque sont les ligne de 20 jusqu'a 29. On vois que l'attaque par **force brute** c'est a dire qui génère par répétitions dfifférents login, modifie les valeurs saisie dans le dictionnaire. Cela va permettre dans un lapse de temps d'envoyer beaucoup de requetes différentes.

----------------------------------------------------

### | Travail à faire 2 - Énumération des logins en mode sécurisé et analyse du code source |

 - [x] **Q1**. Fermer puis relancer BurpSuite. Positionner le niveau de sécurité à 5 et relancer l’attaque en
suivant **les étapes 2 à 4.**

--------------------------------------------------

![Q2](B1_Systeme/img/Owasp2T2.png)
--------------------------------
![Q2](B1_Systeme/img/Owasp2T22.png)

---------------------------------------------------

 - [x] **Q2**. Les informations affichées par le comparateur sont-elles exploitables pour tenter une
**énumération** ?

- Les informations affichées par le comparateur ne sont pas exploitables pour tenter une 
énumération. Ici, on peut voir qu’avec un niveau de sécurité élevé à 5 l'envoie des  
login a répétition est impossible. La sécurité niveau 5 permet donc d'éviter cette attaque.

-----------------------------------------------

 - [x] **Q3**. Chercher dans le code source de la page ws-user-account.php (située dans
/var/www/html/mutillidae/webservices/soap/) le codage mis en place permettant d’obtenir un
encodage sécurisé. **Expliquer le rôle de l’instruction EncodeforHTML.**

-----------------------------------

![Q2](B1_Systeme/img/Finowasp2.png)

-----------------------------------
- Cette Fonction Encode une chaîne d'entrée pour une sortie HTML sécurisée afin d'empêcher les attaques. 

-----------------------------








