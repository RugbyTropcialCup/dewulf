public class Voyageur
{
	private String					nom;
	private Bagage					bagage;
	private final AdressePostale 	adresse;


	/* CONSTRUCTEUR */
	public Voyageur(String p_nom, AdressePostale p_adresse)
	{
		this.nom		= p_nom;
		this.adresse 	= p_adresse;
	}
	
	/* OVERRIDE */
	@Override
	public String toString()
	{
		return "> Informations générales\n" +
				"    Nom :           " + this.nom + "\n\n" +
				this.adresse + "\n\n" +
				(this.bagage != null ? this.bagage + "\n\n" : "");
	}

	/* SETTER */
	/* Mets à jour la propriété < nom > de la classe < Voyageur > */
	public void setNom(String p_nom)
	{
		this.nom = p_nom;
	}

	/* Mets à jour l'ensemble des propriétés de la classe < AdressePostal > */
	public void setAdresse(String p_voie, String p_ville, int p_codePostal)
	{
		this.adresse.setVoie(p_voie);
		this.adresse.setVille(p_ville);
		this.adresse.setCodePostal(p_codePostal);

		System.out.println("/* Mise à jour de l'ensemble de l'adresse postal du voyageur avec succès. */\n\n");
	}

	/* Mets à jour la propriété < voie > de la classe < AdressePostal > */
	public void setAdresseVoie(String p_voie)
	{
		this.adresse.setVoie(p_voie);

		System.out.println("/* Mise à jour de la voie de l'adresse postal du voyageur avec succès. */\n\n");
	}

	/* Mets à jour la propriété < ville > de la classe < AdressePostal > */
	public void setAdresseVille(String p_ville)
	{
		this.adresse.setVille(p_ville);

		System.out.println("/* Mise à jour de la ville de l'adresse postal du voyageur avec succès. */\n\n");
	}

	/* Mets à jour la propriété < codePostal > de la classe < AdressePostal > */
	public void setAdresseCodePostal(int p_codePostal)
	{
		this.adresse.setCodePostal(p_codePostal);

		System.out.println("/* Mise à jour du code postal de l'adresse postal du voyageur avec succès. */\n\n");
	}

	/* METHODE DE VOYAGEUR */
	/* Création d'une nouvelle instance < Bagage > avec les informations passées en paramètre */
	public void ajouteBagage(int p_numero, String p_couleur, int p_poids)
	{
		if (this.bagage == null)
		{
			this.bagage = new Bagage(p_numero, p_couleur, p_poids);

			System.out.println("/* Ajout du bagage au voyageur avec succès. */\n\n");
		}
		else
			System.out.println("/* Ajout du bagage au voyageur impossible, un bagage existe déjà. */\n\n");
	}

	/* Supprime l'instance < Bagage > */
	public void supprimeBagage()
	{
		this.bagage = null;
	}
}
