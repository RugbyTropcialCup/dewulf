import static java.lang.Integer.parseInt;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;


public class Agence_Class
{
	protected String                        Nom;
	protected String                        Adresse;
	protected ArrayList<Voyageur_Class>     VoyageurCollection;


	/* CONSTRUCTOR */
	public Agence_Class(String p_Nom, String p_Adresse)
	{
		Nom                 = p_Nom;
		Adresse             = p_Adresse;

		VoyageurCollection  = new ArrayList<Voyageur_Class>();
	}

	public Agence_Class(String p_Nom, String p_Adresse, ArrayList<Voyageur_Class> p_VoyageurCollection)
	{
		Nom                 = p_Nom;
		Adresse             = p_Adresse;

		VoyageurCollection  = p_VoyageurCollection;
	}

	/* SPECIFIC CLASS METHOD */
	public void AfficherInfoAgence()
	{
		System.out.println("__________________________________");
		System.out.println("#       INFORMATION AGENCE       #");
		System.out.println("__________________________________\n");
		System.out.println("Nom     : " + Nom);
		System.out.println("Adresse : " + Adresse + "\n");

		System.out.println("Liste des voyageurs :");
		for (Voyageur_Class Voyageur : VoyageurCollection)
		{
			System.out.println("  - " + Voyageur.Nom);
		}
		System.out.println();
	}
	
	
	public String getNom(){ 
        return this.Nom;
    }
    public void setNom(String nom){
        this.Nom = Nom;
    }
	
	public String getAdresse(){ 
        return this.Adresse;
    }
    public void setAdresse(String Adresse){
        this.Adresse = Adresse;
    }


	public void AjouterVoyageur(String p_Nom)
	{
		Voyageur_Class Voyageur = new Voyageur_Class(p_Nom);

		VoyageurCollection.add(Voyageur);
	}

	public void AjouterListeVoyageur()
	{
		Voyageur_Class  Voyageur;
		Scanner         Scanner;
		String          Input;
		int             BoucleMax;


		Scanner = new Scanner(System.in);
		System.out.println("Combien de voyageur voulez-vous ajouter ?");
		System.out.print("> ");
		Input = Scanner.nextLine();
		System.out.println();

		BoucleMax = parseInt(Input);

		for (int Indice = 0; Indice < BoucleMax; Indice++)
		{
			Scanner = new Scanner(System.in);
			System.out.println("Nom du voyageur " + (Indice + 1) + " ?");
			System.out.print("> ");
			Input = Scanner.nextLine();
			System.out.println();

			AjouterVoyageur(Input);
		}
	}

	public void ChercheVoyageur()
	{
		boolean         Afficher = false;
		Scanner         Scanner;
		String          Input;


		Scanner = new Scanner(System.in);
		System.out.println("Indiquer le nom du voyageur à chercher :");
		System.out.print("> ");
		Input = Scanner.nextLine();
		System.out.println();

		for (Voyageur_Class Voyageur : VoyageurCollection)
		{
			if (!Afficher && Objects.equals(Voyageur.Nom, Input))
			{
				Voyageur.AfficherVoyageur();
				Afficher = true;
			}
		}

		if (!Afficher)
			System.out.println("Le voyageur que vous cherchez n'existe pas dans la liste !\n");
	}

	public void SupprimerVoyageurParNom()
	{
		Scanner         Scanner;
		String          Input;
		int				Compteur;


		Scanner = new Scanner(System.in);
		System.out.println("Indiquer le nom du voyageur à supprimer :");
		System.out.print("> ");
		Input = Scanner.nextLine();
		System.out.println();

		Compteur = 0;
		for (Voyageur_Class Voyageur : VoyageurCollection)
		{
			if (Objects.equals(Voyageur.Nom, Input))
				VoyageurCollection.remove(Compteur);

			Compteur++;
		}
	}
}
