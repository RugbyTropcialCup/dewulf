import java.util.Scanner;

public class Main
{
	public static void main(String[] args)
	{
		Scanner Scanner;
		String  Input;


		Agence_Class Agence = new Agence_Class("Eric", "27 Rue Saint-Honoré, Paris 75018");

		Agence.AjouterVoyageur("Quentin");
		Agence.AjouterVoyageur("Urne");
		Agence.AjouterVoyageur("Chasu");
		Agence.AfficherInfoAgence();

        Agence.AjouterListeVoyageur();
        Agence.AfficherInfoAgence();

//		Agence.ChercheVoyageur();

		Agence.SupprimerVoyageurParNom();
		Agence.AfficherInfoAgence();
	}
}
