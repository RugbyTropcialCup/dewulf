*Dewulf Mael* 

*JavaFx Mission 7/8*

*20/02/2023*

# Que faire ? 

-  Quelle classe je démarre ? --> HelloApplication.java dans JavaFx

- Quelle fichier (classes) ont été modifié(e)s ?

Pour le fichier JavaFx :

- InsertVoyageurFormController
- HelloControlleur

Pour le fichier JDBC : 

- Tous les fichier ( Fichier dao et model)

## Image de Scene Builer : 

![imgscb](img/Capture_d_écran_2023-02-20_142124.png)

![imgbbdd](img/Capture_d_écran_2023-02-20_142025.png)



