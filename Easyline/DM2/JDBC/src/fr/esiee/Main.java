package fr.esiee;

import java.sql.*;

public class Main {
    public static void main(String[] args) {

        System.out.println("Mission JDBC");
        String urlBDD = "jdbc:mysql://localhost:3306/easyline";
        String user = "root";
        String pwd = "root";
        String reqInsertAdresse = "INSERT INTO AdressePostales (numVoie, ville, codePostal) VALUES ('5 rue de Cergy', 'Osny', '95520')";
        String reqInsertVoyageur = "INSERT INTO Voyageurs (nom, prenom, anneeNaiss, adresse, type) VALUES ('Dewulf', 'Mael', 2003, 1, 'normal')";
        String reqSelectAdresse = "SELECT id, numVoie, codePostal, ville FROM AdressePostales";

        try {
            // Récupération d'un objet de type Connexion : son nom "conn"
            Connection conn = DriverManager.getConnection(urlBDD, user, pwd); // Nécessite le driver dans le classpath
            System.out.println("Connection OK.");

            // Requête insertion
            Statement fluxreq = conn.createStatement();

            // Executer requête INSERT via flux - Requête Adresse
            int res = fluxreq.executeUpdate(reqInsertAdresse);
            System.out.println("Nombre de lignes MAJ : " + res);

            // Executer requête INSERT via flux - Requête Voyageur
            int voy = fluxreq.executeUpdate(reqInsertVoyageur);
            System.out.println("Nombre de lignes MAJ : " + voy);

            // Executer requête SELECT via flux - Select Adresse
            ResultSet indexRes = fluxreq.executeQuery(reqSelectAdresse);
            while(indexRes.next()){ //Double fonction bouger l'index vers le suivant ; si ça marche return true

                System.out.println(" ——————————————");
                System.out.println("| Informations |");
                System.out.println(" ——————————————");
                System.out.println("| id n°" + indexRes.getInt(1));
                System.out.println("| Ville : " + indexRes.getString(4));
                System.out.println("");
            }


        } catch (SQLException e) {
            // throw new RuntimeException(e);
            System.out.println("Problème.");
            e.printStackTrace(); // Affichage de la pile d'exécution qui a causé l'erreur
        }
    }
}
