package fr.esiee.javafxtest;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

//on a un controleur par panel fxml
//gestion d'événement géré ici
public class HelloController {
    @FXML
    private Label welcomeText;

    @FXML
    protected void onHelloButtonClick() {
        welcomeText.setText("Welcome to JavaFX Application!");
        System.out.println("C'est bon, j'ai cliqué !!");
    }
    //méthode dans contrôleur utilisé ici
}