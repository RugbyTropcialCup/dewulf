package fr.esiee.javafxtest;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

// Application que l'on doit démarrer
// Ici on charge le premier fxml qui sera le panel root de l'application
public class HelloApplication extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        // On charge le fxml
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("insertVoyageurForm-view.fxml"));
        // Load permet de charger le panel décrit dans le fxml et le controller
        Scene scene = new Scene(fxmlLoader.load(), 600, 400);
        stage.setTitle("Bienvenue dans mon app !");
        stage.setScene(scene);
        // Ici on affiche la fenêtre
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}