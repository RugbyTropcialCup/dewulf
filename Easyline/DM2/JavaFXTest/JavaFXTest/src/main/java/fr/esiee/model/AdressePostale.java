package fr.esiee.model;

public class AdressePostale {
	private String voie;
	private String ville;
	private String codePostal;

	// Initialisation d'un constructeur à 0 argument
	public AdressePostale() {

	}

	// Initialisation d'un constructeur à 3 arguments
	public AdressePostale(String voie, String ville, String codePostal) {
		setVoie(voie);
		setVille(ville);
		setCodePostal(codePostal);
	}

	// Initialisation des Getters
	String getVoie() {
		return this.voie;
	}

	String getVille() {
		return this.ville;
	}

	String getCodePostal() {
		return this.codePostal;
	}

	// Initialisation des Setters
	public void setVoie(String voie) {
		this.voie = voie;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	// Initialisation de la méthode afficher pour l'affichage de l'adresse postale
	private void afficher() {
		System.out.println(
				"\nAdresse postale : "
						+ this);
	}

	// Initialisation du Override pour les carastéristique de l'adresse postale
	@Override
	public String toString() {
		return getVoie() + ", "
				+ getCodePostal() + " - "
				+ getVille();
	}

	public static void main(String[] args) {

		// Création d'un bagage
		AdressePostale adressePostale = new AdressePostale("5 rue de Cergy", "Lille", "59000");
		adressePostale.afficher();

		// Modification du bagage avec le méthodes SET et le réaffiche
		adressePostale.setVoie("8 Rue Pierre de Coubertin");
		adressePostale.setVille("Pontoise");
		adressePostale.setCodePostal("95300");
		adressePostale.afficher();
		System.out.println("\n");
	}
}
