import java.util.Scanner;
public class Question4{
  public static void main (String[] args){
    Scanner sc = new Scanner(System.in);                    
    String mdp = "Luxxx";                                         /* création du mod de passe*/
    System.out.println("Tapez le mot de passe");                /*demande a l'utilisateur le mdp*/
    String mdp1 = sc.nextLine();                                    /*création de la variable mdp1*/
    if ( mdp1.equals(mdp)){                                     /*si la variable corespond alors*/
      System.out.println("Bienvenue");                          /*message si le code est bon*/
      int carte;
      Double remise = 0.0;
      System.out.println("Quel est votre abonnement 1=Aucun 2=Gold 3=Platinium ?"); 
      carte = sc.nextInt();
      System.out.println("Quel est la marque du véhicule ?");    /*demande à l'utilisateur la marque du véhicule*/
      String vhmarque = sc.nextLine();                           /* création de la variable "vhmarque" */
      System.out.println("Quel est le modèle du véhicule ?");    /*demande à l'utilisateur le modèle du véhicule */
      String vhmodele = sc.nextLine();                           /*création de la variable "vhmodele */
      System.out.println("Véhicule électrique ? false = Non true = Oui"); /* demande à l'utilisateur si le véhicule est éléctrique */
      boolean véléctrique = sc.nextBoolean();                          /*création de la variable "véléctrique */
      System.out.println("Quel est le prix HT du véhicule ?");  /* demande à l'utilisateur le prix HT du véhicule */
      double prixht = sc.nextDouble();                               /* création de la variable "prixht" */
      double tauxtva = 1.2;
      if (véléctrique == true){                                            /* si voiture est vrai(donc élétrique)*/
          tauxtva = 1.05;                                     /* alors la tva sera de 5% sinon elle reste a 20%*/
      }
      double prixttc = prixht * tauxtva; /* formule de calcul pour le prixttc en fonction du prixht et du tauxtva */
      if(carte==2 && véléctrique == true){                                                 /*Carte gold*/
            remise = 0.3;
            System.out.println("Remise Gold avec voiture électrique : "+ (prixttc*remise) + " €");
      }if(carte==2 && véléctrique == false){
            remise = 0.2;
            System.out.println("Remise Gold : "+ (prixttc*remise) + " €");
      }if(carte==3){
        remise = 0.15;
        System.out.println("Remise Platinium : "+ (prixttc*remise) + " €");
      }
      
      System.out.println("La marque du véhicule est : "+ vhmarque );      /* affichage de la marque du véhicule */
      System.out.println("Le modèle du véhicule est : "+ vhmodele );      /* affichage du modèle du véhicule */
      System.out.println("Le prix TTC est de : "+ prixttc );              /* affichage du prix TTC */
      double prixremise = prixttc-(prixttc*remise);
      if (prixttc > 20000) {                                                  /* si le prix est supérieur a 20 000  */
        double dernierprix = prixremise * 0.9;                                  /* alors mettre remise a 10%  */
        System.out.println("Prix avec remise : " + dernierprix );            /* calcul de la remise */
      }else{                             
        System.out.println("Voici le prix avec remise : " + prixremise);       /* affichage du prix avec remise */
      }
    
    }else{
      System.out.println("Mot de passe incorrect");       /*sinon mettre "mdp incorect" et pas d'accès*/
    }
  }
}
