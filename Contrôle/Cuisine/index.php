<?php 
require 'vendor/autoload.php';
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;
$app->get('/gateaux', function(Request $request, Response $response){  
  return getGateaux();
});

$app->post('/gateaux', function(Request $request, Response $response){
    $id = $request->getAttribute('id');
         return getGateaux();
  });

$app->get('/gateau', function(Request $request, Response $response){
    $tb = $request->getQueryParams();
    $nom = $tb["nom"];
    $couleur = $tb["couleur"];
    $ingredient_p = $tb["ingredient_p"];
    $date_f = $tb["date_f"];
    $poids = $tb["poids"];
    return postGateau($nom,$couleur,$ingredient_p,$date_f,$poids);
  });
  
  // $app->post('/gateau', function(Request $request, Response $response){
  //   $nom = $_POST["nom"];
  //   $couleur = $_POST["couleur"];
  //   $poids = $_POST["ingredient_p"];
  //   $couleur = $_POST["date_f"];
  //   $poids = $_POST["poids"];
  //   return post Gateau($nom,$couleur,$ingredient_p,$date_f,$poids);
  // });

  function connexion()
  {
     return $dbh = new PDO("mysql:host=localhost;dbname=cuisine", 'root', 'root', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',PDO::ATTR_ERRMODE=>PDO::ERRMODE_WARNING));
  }
  function getGateaux()
  {
    $sql = "SELECT * FROM gateau";
    try{
      $dbh=connexion();
      $statement = $dbh->prepare($sql);
      $statement->execute();
       $result = $statement->fetchAll(PDO::FETCH_CLASS); 
                   return json_encode($result, JSON_PRETTY_PRINT);
    } catch(PDOException $e){
      return '{"error":'.$e->getMessage().'}';
    }
  }

  function postGateau($nom,$couleur,$ingredient_p,$date_f,$poids)
  {
    $sql = "INSERT INTO gateau (nom, couleur, ingredient_p, date_f, poids)
    VALUES (:nom, :couleur, :ingredient_p, :date_f, poids)";
    try{
      $dbh=connexion();
      $dbh->errorInfo();
      $statement = $dbh->prepare($sql);
      $statement->bindParam(":nom", $nom);
      $statement->bindParam(":couleur", $couleur);
      $statement->bindParam(":ingredient_p", $ingredient_p);
      $statement->bindParam(":date_f", $date_f);
      $statement->bindParam(":poids", $poids);
      $a=$statement->execute();
          // if ($a == false){
          //     http_response_code(500);
          //  }
      return  $sql;
        // return json_encode($result, JSON_PRETTY_PRINT);
    } catch(PDOException $e){
      return '{"error":'.$e->getMessage().'}';
    }
  }
